module.exports = {
  theme: {
    extend: {
      fontFamily: {
        display: 'Playfair Display, serif',
        body: 'Roboto, sans-serif'
      },
      boxShadow: {
        subtle: '0 0 15px -2px rgba(0,0,0,.1)',
        outline: '0 0 0 3px #276749 !important;'
      },
      colors: {
        'gray-light': '#ECECEC',
        'gray-dark': '#595959',
        'gray-bg': '#FAFAFA',
        'highlight': '#276749'
      },
      borderRadius: {
        'post-row': '3rem 0.25rem 0.25rem 3rem',
        'post-col': '0.25rem 0.25rem 3rem 3rem',

      },
      height: {
        200: '200px',
        300: '300px',
        400: '400px',
        500: '500px',
        600: '600px',
        700: '700px',
      },
      spacing: {
        200: '200px',
        250: '250px',
        300: '300px',
        350: '350px',
        400: '400px',
        450: '450px',
        500: '500px',
        550: '550px',
        600: '600px',
        650: '650px',
        700: '700px',
        750: '750px',
      }
    }
  },
  variants: {
    cursor: ['hover'],
  },
  plugins: []
}


// bg-primary FAFAFA