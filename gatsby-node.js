/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it
const path = require('path');

// Implement the Gatsby API “createPages”. This is called once the
// data layer is bootstrapped to let plugins create pages from data.
exports.createPages = async ({ graphql, actions, reporter }) => {
    const { createPage } = actions
    // Query for markdown nodes to use in creating pages.
    const result = await graphql(
        `
        query Contentful {
            pages: allContentfulPage {
            nodes {
                slug
                id
                }
            }
            categories: allContentfulCategory {
              nodes {
                name
                slug
                id
              }
            }
            posts: allContentfulPost {
              nodes {
                id
                slug
              }
            }
        }
      `
    )
    // Handle errors
    if (result.errors) {
      reporter.panicOnBuild(`Error while running GraphQL query.`)
      return
    }

    // Page Templates
    const PageTemplate = path.resolve(`src/templates/page.template.js`)
    const CategoryTemplate = path.resolve(`src/templates/category.template.js`)
    const PostTemplate = path.resolve(`src/templates/post.template.js`)


    result.data.pages.nodes.forEach(({ slug, id }) => {
      createPage({
        path: slug,
        component: PageTemplate,
        // In your blog post template's graphql query, you can use path
        // as a GraphQL variable to query for data from the markdown file.
        context: {
          id: id,
        },
      })
    })
    result.data.categories.nodes.forEach(({ slug, id, name }) => {
      createPage({
        path: slug,
        component: CategoryTemplate,
        // In your blog post template's graphql query, you can use path
        // as a GraphQL variable to query for data from the markdown file.
        context: {
          id: id,
          name: name
        },
      })
    })
    result.data.posts.nodes.forEach(({ slug, id }) => {
      createPage({
        path: `posts/${slug}`,
        component: PostTemplate,
        // In your blog post template's graphql query, you can use path
        // as a GraphQL variable to query for data from the markdown file.
        context: {
          id: id,
        },
      })
    })
  }