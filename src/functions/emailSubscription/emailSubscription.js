const axios = require("axios");

// Docs on event and context https://www.netlify.com/docs/functions/#the-handler-method
exports.handler = async (event, context) => {
  try {
    let body = {};

    // Make sure the request has data
    if (!event.body) {
      return { statusCode: 401, body: JSON.stringify({ message: "Unauthorised access. CODE: 1" }) }
    }
    try {
      body = await JSON.parse(event.body)
      let { email, name, consent } = body



      // Make sure the correct parameters are provided
      if (typeof email !== 'string' || typeof name !== 'string' || typeof consent === 'string') {
        return { statusCode: 401, body: JSON.stringify({ message: "Unauthorised access. CODE: 2" }) }
      }

      const options = {
        headers: {
          accept: "application/json",
          "content-type": "application/json",
        }
      };

      const axiosBody = JSON.stringify({
        value1: body.email,
        value2: body.name,
        value3: body.consent
      })


      const res = await axios.post('https://maker.ifttt.com/trigger/CPHFPVVHBL/with/key/dqHdOo2NhJ5rA7LM7EUDfC', axiosBody, options)

      if (!res.status >= 200 && res.status < 300) {
        // NOT res.status >= 200 && res.status < 300
        return { statusCode: res.status, body: JSON.stringify({ message: res.statusText, success: false }) };
      }
      if (res.status === 200) {
        return {
          statusCode: 200,
          body: JSON.stringify({ success: true, message: "Successful registration." }),
        }
      }

      return {
        statusCode: 401, success: false, body: JSON.stringify({ message: "Unauthorised access. CODE: 3", success: false })
      }

    } catch (err) {
      return { statusCode: 500, body: JSON.stringify({ message: "There was an error", success: false }) }
    }
  } catch (err) {
    console.log(err.toString())
    return { statusCode: 500, body: err.toString() }
  }
}
