import "./src/css/global.css"
import "./src/css/font-awesome/all.css"
import "./src/css/custom.css"


export const onRouteUpdate = ({ location, prevLocation }) => {
    if (location && location.state)
      location.state.referrer = prevLocation ? prevLocation.pathname : null
  }